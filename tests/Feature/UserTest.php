<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use Illuminate\Testing\Fluent\AssertableJson;
use App\Models\User;
use Illuminate\Support\Str;
use Config;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void 
    {
        parent::setUp();
        
        \Artisan::call('passport:install',['-vvv' => true]);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testRegister()
    {
        $user = User::factory()->raw();

        $response = $this->post( 'api/register', $user );

        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('token')
        );
    }

    public function testLogin()
    {
        $user = User::create([
            'name' => 'sidiqadi',
            'email' => 'sidiqadi7@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('password'), // password
            'remember_token' => Str::random(10),
        ]);
        
        $response = $this->post( 'api/login', ['email'=>$user->email, 'password'=> 'password'] );

        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('token')
        );
    }
}
