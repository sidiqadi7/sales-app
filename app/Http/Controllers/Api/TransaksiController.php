<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Services\TransaksiService;
use App\Http\Requests\TransaksiShowRequest;
use App\Http\Requests\TransaksiPostRequest;
use App\Http\Requests\TransaksiPutRequest;
use App\Http\Requests\TransaksiReportRequest;
use App\Http\Resources\TransaksiResource;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class TransaksiController extends Controller
{
    private TransaksiService $transaksiService;

    public function __construct(TransaksiService $transaksiService) 
    {
        $this->transaksiService = $transaksiService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): JsonResponse
    {
        $data = $this->transaksiService->get();

        return response()->json(TransaksiResource::collection($data), HttpResponse::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransaksiPostRequest $request)
    {
        $data = $this->transaksiService->create($request->all());

        return response()->json(new TransaksiResource($data), HttpResponse::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->transaksiService->show($id);

        return response()->json(new TransaksiResource($data), HttpResponse::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TransaksiPutRequest $request, $id)
    {
        $data = $this->transaksiService->update($request->all(), $id);

        return response()->json(new TransaksiResource($data), HttpResponse::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->transaksiService->delete($id);

        return response()->json(new TransaksiResource($data), HttpResponse::HTTP_OK);
    }

    public function report(TransaksiReportRequest $request){
        $data = $this->transaksiService->report($request->all());

        return response()->json(new TransaksiResource($data), HttpResponse::HTTP_OK);
    }
}
