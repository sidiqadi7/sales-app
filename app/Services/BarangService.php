<?php
namespace App\Services;

use App\Models\Barang;

class BarangService{
    public function get(){
        $data = Barang::with('user', 'supplier')->get();
        return $data;
    }

    public function create(array $dataInput){
        $data = Barang::firstOrCreate($dataInput);
        
        return $data->with('user', 'supplier')->latest()->first()->makeHidden(['user_id', 'supplier_id']);
    }

    public function update(array $dataInput, $id){
        $data = Barang::where('id', $id);
        $data->update($dataInput);
        
        return $data->with('user', 'supplier')->first()->makeHidden(['user_id', 'supplier_id']);
    }

    public function show($id){
        $data = Barang::where('id', $id);
        
        return $data->with('user', 'supplier')->first()->makeHidden(['user_id', 'supplier_id']);
    }

    public function delete($id){
        $data = Barang::where('id', $id);
        $data->delete();
        
        return $data->first();
    }
}