<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Services\SupplierService;
use App\Http\Requests\SupplierShowRequest;
use App\Http\Requests\SupplierPostRequest;
use App\Http\Requests\SupplierPutRequest;
use App\Http\Resources\SupplierResource;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class SupplierController extends Controller
{
    private SupplierService $supplierService;

    public function __construct(SupplierService $supplierService) 
    {
        $this->supplierService = $supplierService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): JsonResponse
    {
        $data = $this->supplierService->get();

        return response()->json(SupplierResource::collection($data), HttpResponse::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SupplierPostRequest $request)
    {
        $data = $this->supplierService->create($request->all());

        return response()->json(new SupplierResource($data), HttpResponse::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->supplierService->show($id);

        return response()->json(new SupplierResource($data), HttpResponse::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SupplierPutRequest $request, $id)
    {
        $data = $this->supplierService->update($request->all(), $id);

        return response()->json(new SupplierResource($data), HttpResponse::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->supplierService->delete($id);

        return response()->json(new SupplierResource($data), HttpResponse::HTTP_OK);
    }
}
