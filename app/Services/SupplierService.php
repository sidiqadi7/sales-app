<?php
namespace App\Services;

use App\Models\Supplier;

class SupplierService{
    public function get(){
        $data = Supplier::all();
        return $data;
    }

    public function create(array $dataInput){
        $data = Supplier::firstOrCreate($dataInput);
        
        return $data;
    }

    public function update(array $dataInput, $id){
        $data = Supplier::where('id', $id);
        $data->update($dataInput);
        
        return $data->first();
    }

    public function show($id){
        $data = Supplier::where('id', $id);
        
        return $data->first();
    }

    public function delete($id){
        $data = Supplier::where('id', $id);
        $data->delete();
        
        return $data->first();
    }
}