<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\PassportAuthController;
use App\Http\Controllers\API\SupplierController;
use App\Http\Controllers\API\BarangController;
use App\Http\Controllers\API\TransaksiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('register', [PassportAuthController::class, 'register']);
Route::post('login', [PassportAuthController::class, 'login']);

Route::middleware('auth:api')
    ->group(function () {
        // Supplier endpoints
        Route::get('suppliers', [SupplierController::class, 'index']);
        Route::post('suppliers', [SupplierController::class, 'store']);
        Route::get('supplier/{id}', [SupplierController::class, 'show']);
        Route::put('supplier/{id}', [SupplierController::class, 'update']);
        Route::delete('supplier/{id}', [SupplierController::class, 'destroy']);

        // Barang endpoints
        Route::get('barang', [BarangController::class, 'index']);
        Route::post('barang', [BarangController::class, 'store']);
        Route::get('barang/{id}', [BarangController::class, 'show']);
        Route::put('barang/{id}', [BarangController::class, 'update']);
        Route::delete('barang/{id}', [BarangController::class, 'destroy']);

        // Transaksi endpoints
        Route::get('transaksi', [TransaksiController::class, 'index']);
        Route::get('laporan-transaksi', [TransaksiController::class, 'report']);
        Route::post('transaksi', [TransaksiController::class, 'store']);
        Route::get('transaksi/{id}', [TransaksiController::class, 'show']);
        Route::put('transaksi/{id}', [TransaksiController::class, 'update']);
        Route::delete('transaksi/{id}', [TransaksiController::class, 'destroy']);

    });
