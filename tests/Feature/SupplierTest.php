<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;

use Tests\TestCase;

use App\Models\User;
use App\Models\Supplier;

use Laravel\Passport\Passport;

class SupplierTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void 
    {
        parent::setUp();
        
        \Artisan::call('passport:install',['-vvv' => true]);
        
        Passport::actingAs(
            User::factory()->create(),
        );
    }
    
    public function testGetSupplier()
    {
        Supplier::factory()->create();

        $response = $this->get('api/suppliers');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            [
                'id', 'nama', 'alamat', 'nomor_telepon', 'created_at', 'updated_at'
            ],
        ]);
    }

    public function testCreateSupplier()
    {
        $post = [
            'nama'=>'Didin',
            'alamat'=>'Bandung',
            'nomor_telepon' => 89600888
        ];

        $response = $this->post('api/suppliers', $post);
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->hasAll('id', 'nama', 'alamat', 'nomor_telepon', 'created_at', 'updated_at')
        );

        $this->assertDatabaseHas('suppliers', $post);
    }

    public function testUpdateSupplier()
    {
        $data = Supplier::factory()->create();
        $post = [
            'nama'=>'Didin',
            'alamat'=>'Bandung',
            'nomor_telepon' => 89600888
        ];

        $response = $this->put('api/supplier/'.$data->id, $post);
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->hasAll('id', 'nama', 'alamat', 'nomor_telepon', 'created_at', 'updated_at')
        );

        $this->assertDatabaseHas('suppliers', $post);
    }

    public function testDeleteSupplier()
    {
        $data = Supplier::factory()->create();

        $response = $this->delete('api/supplier/'.$data->id);
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->missing('id', 'nama', 'alamat', 'nomor_telepon', 'created_at', 'updated_at')
        );
    }

    public function testShowSupplier()
    {
        $data = Supplier::factory()->create();

        $response = $this->get('api/supplier/'.$data->id);
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->hasAll('id', 'nama', 'alamat', 'nomor_telepon', 'created_at', 'updated_at')
        );
    }
}
