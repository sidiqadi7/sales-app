<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Illuminate\Testing\Fluent\AssertableJson;

use Tests\TestCase;

use Carbon\Carbon;

use App\Models\User;
use App\Models\Supplier;
use App\Models\Barang;
use App\Models\Transaksi;

use Laravel\Passport\Passport;

class TransaksiTest extends TestCase
{
    use RefreshDatabase;
    protected $user, $supplier, $barang, $transaksi;

    public function setUp(): void 
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->supplier = Supplier::factory()->create();

        $this->barang = Barang::factory()->create([
            'user_id' => $this->user->id,
            'supplier_id' => $this->supplier->id,
        ]);

        $this->transaksi = Transaksi::factory()->create([
            'user_id' => $this->user->id,
            'barang_id' => $this->barang->id,
        ]);

        \Artisan::call('passport:install',['-vvv' => true]);

        Passport::actingAs(
            $this->user
        );
    }

    public function testGetTransaksi()
    {
        $response = $this->get('api/transaksi');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            [
                'user', 
                'barang', 
                'jumlah', 
                'tanggal', 
                'harga_jual'
            ],
        ]);
    }

    public function testCreateTransaksi()
    {
        $post = [
            'user_id' => $this->user->id, 
            'barang_id' => $this->barang->id, 
            'jumlah' => 10, 
            'tanggal' => new Carbon('2023-01-23'), 
            'harga_jual' => 8000
        ];

        $response = $this->post('api/transaksi', $post);
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->hasAll('id', 'jumlah', 'tanggal', 'harga_jual', 'user', 'barang', 'created_at', 'updated_at')
        );

        $this->assertDatabaseHas('transaksi', $post);
    }

    public function testUpdateTransaksi()
    {
        $post = [
            'user_id' => $this->user->id, 
            'barang_id' => $this->barang->id, 
            'jumlah' => 15, 
            'tanggal' => new Carbon('2023-01-23'), 
            'harga_jual' => 10000
        ];

        $response = $this->put('api/transaksi/'.$this->transaksi->id, $post);
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->hasAll('id', 'user', 'barang', 'jumlah', 'tanggal', 'harga_jual', 'created_at', 'updated_at')
        );

        $this->assertDatabaseHas('transaksi', $post);
    }

    public function testDeleteTransaksi()
    {
        $response = $this->delete('api/transaksi/'.$this->transaksi->id);
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->missing('id', 'user', 'barang', 'jumlah', 'tanggal', 'harga_jual', 'created_at', 'updated_at')
        );
    }

    public function testShowTransaksi()
    {
        $response = $this->get('api/transaksi/'.$this->transaksi->id);
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->hasAll('id', 'user', 'barang', 'jumlah', 'tanggal', 'harga_jual', 'created_at', 'updated_at')
        );
    }
}
