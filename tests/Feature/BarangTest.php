<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Illuminate\Testing\Fluent\AssertableJson;

use Tests\TestCase;

use App\Models\User;
use App\Models\Supplier;
use App\Models\Barang;

use Laravel\Passport\Passport;

class BarangTest extends TestCase
{
    use RefreshDatabase;
    protected $user, $supplier, $barang;

    public function setUp(): void 
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->supplier = Supplier::factory()->create();
        $this->barang = Barang::factory()->create([
            'user_id' => $this->user->id,
            'supplier_id' => $this->supplier->id,
        ]);

        \Artisan::call('passport:install',['-vvv' => true]);

        Passport::actingAs(
            $this->user
        );
    }

    public function testGetBarang()
    {
        $response = $this->get('api/barang');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            [
                'kode_barang',
                'nama',
                'user',
                'supplier'
            ],
        ]);
    }

    public function testCreateBarang()
    {
        $post = [
            'user_id'=> $this->user->id,
            'supplier_id'=> $this->supplier->id,
            'kode_barang' => Str::random(10),
            'nama' => Str::random(10)
        ];

        $response = $this->post('api/barang', $post);
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->hasAll('id', 'kode_barang', 'nama', 'user', 'supplier', 'created_at', 'updated_at')
        );

        $this->assertDatabaseHas('barang', $post);
    }

    public function testUpdateBarang()
    {
        $post = [
            'user_id'=> $this->user->id,
            'supplier_id'=> $this->supplier->id,
            'kode_barang' => Str::random(10),
            'nama' => Str::random(10)
        ];

        $response = $this->put('api/barang/'.$this->barang->id, $post);
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
        $json->hasAll('id', 'kode_barang', 'nama', 'user', 'supplier', 'created_at', 'updated_at')
        );

        $this->assertDatabaseHas('barang', $post);
    }

    public function testDeleteBarang()
    {
        $response = $this->delete('api/barang/'.$this->barang->id);
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->missing('id', 'kode_barang', 'nama', 'user', 'supplier', 'created_at', 'updated_at')
        );
    }

    public function testShowBarang()
    {
        $response = $this->get('api/barang/'.$this->barang->id);
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->hasAll('id', 'kode_barang', 'nama', 'user', 'supplier', 'created_at', 'updated_at')
        );
    }
}
