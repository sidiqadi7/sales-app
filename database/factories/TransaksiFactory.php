<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Barang;
use App\Models\Supplier;
use App\Models\Transaksi;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TransaksiFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Transaksi::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory(), 
            'barang_id' => Barang::factory() , 
            'jumlah' => $this->faker->numberBetween(1,100), 
            'tanggal' => $this->faker->date('Y_m_d'), 
            'harga_jual' => $this->faker->numberBetween(100,500000)
        ];
    }
}
