<?php
namespace App\Services;

use App\Models\Transaksi;
use App\Models\Barang;
use Auth;

class TransaksiService{
    public function get(){
        $data = Transaksi::with(['barang', 'user'])->get();
        return $data;
    }

    public function create(array $dataInput){
        $dataInput['user_id'] = Auth::id();
        
        $data = Transaksi::firstOrCreate($dataInput);
        
        return $data->with(['barang', 'user'])->first()->makeHidden(['barang_id', 'user_id']);
    }

    public function update(array $dataInput, $id){
        $data = Transaksi::where('id', $id);
        $data->update($dataInput);
        
        return $data->with(['barang', 'user'])->first()->makeHidden(['barang_id', 'user_id']);
    }

    public function show($id){
        $data = Transaksi::where('id', $id);
        
        return $data->with(['barang', 'user'])->first()->makeHidden(['barang_id', 'user_id']);
    }

    public function delete($id){
        $data = Transaksi::where('id', $id);
        $data->delete();
        
        return $data->first();
    }

    public function report(array $dataInput){
        $data = Transaksi::with('barang')->whereBetween('tanggal', [$dataInput['date_start'], $dataInput['date_end']])->get();
        // $data->total_barang_terjual = $data->sum('jumlah');
        // $data->total_omset_penjualan = $data->sum('harga_jual');
        
        // dd();
        return [
                'transaksi' => $data,
                'summary' => [
                    'total_barang_terjual' => $data->sum('jumlah'),
                    'total_omset_penjualan' =>$data->sum('harga_jual')
                ],
            ];
        // return $data;
    }
}