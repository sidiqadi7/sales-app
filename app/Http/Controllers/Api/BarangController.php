<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Services\BarangService;
use App\Http\Requests\BarangShowRequest;
use App\Http\Requests\BarangPostRequest;
use App\Http\Requests\BarangPutRequest;
use App\Http\Resources\BarangResource;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class BarangController extends Controller
{
    private BarangService $barangService;

    public function __construct(BarangService $barangService) 
    {
        $this->barangService = $barangService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): JsonResponse
    {
        $data = $this->barangService->get();

        return response()->json(BarangResource::collection($data), HttpResponse::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BarangPostRequest $request)
    {
        $data = $this->barangService->create($request->all());

        return response()->json(new BarangResource($data), HttpResponse::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->barangService->show($id);

        return response()->json(new BarangResource($data), HttpResponse::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BarangPutRequest $request, $id)
    {
        $data = $this->barangService->update($request->all(), $id);

        return response()->json(new BarangResource($data), HttpResponse::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->barangService->delete($id);

        return response()->json(new BarangResource($data), HttpResponse::HTTP_OK);
    }
}
