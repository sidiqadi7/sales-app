<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Barang;
use App\Models\Supplier;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class BarangFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Barang::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id'=>User::factory(),
            'supplier_id'=>Supplier::factory(),
            'kode_barang' => Str::random(10),
            'nama' => Str::random(10)
        ];
    }
}
